<?php
$start = microtime(true); 

$ctx = stream_context_create(array( 
    'http' => array( 
        'timeout' => 5 
        ) 
    ) 
); 

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip=$_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

$ip = getRealIpAddr();

if(!empty($_GET["ipToCheck"])){
	$ip = $_GET["ipToCheck"];
}

$temp = [];

// Get geolocation data from db-ip.com https://db-ip.com/api/free
$db_ip_keys=['key_0', 'key_1','key_2','key_3'];
$db_ip_key = $db_ip_keys[array_rand($db_ip_keys)];
$db_ip = json_decode(file_get_contents('http://api.db-ip.com/v2/'.$db_ip_key.'/'.$ip, 0, $ctx), true);
$temp[0]['city'] = isset($db_ip['city']) ? $db_ip['city'] : '';
$temp[0]['region_name'] = isset($db_ip['stateProv']) ? $db_ip['stateProv'] : '';
$temp[0]['country_name'] = isset($db_ip['countryName']) ? $db_ip['countryName'] : '';
$temp[0]['country_code'] = isset($db_ip['countryCode']) ? $db_ip['countryCode'] : '';

// Get geolocation data from ip-api.com
$ip_api = json_decode(file_get_contents('http://ip-api.com/json/'.$ip.'?fields=country,countryCode,region,regionName,city,lat,lon,timezone,isp,org,as&lang=en', 0, $ctx), true);
$temp[1]['city'] = isset($ip_api['city']) ? $ip_api['city'] : '';
$temp[1]['region_name'] = isset($ip_api['regionName']) ? $ip_api['regionName'] : '';
$temp[1]['country_name'] = isset($ip_api['country']) ? $ip_api['country'] : '';
$temp[1]['country_code'] = isset($ip_api['countryCode']) ? $ip_api['countryCode'] : '';
$temp[1]['timezone'] = isset($ip_api['timezone']) ? $ip_api['timezone'] : '';
$temp[1]['latitude'] = isset($ip_api['lat']) ? $ip_api['lat'] : '';
$temp[1]['longitude'] = isset($ip_api['lon']) ? $ip_api['lon'] : '';
$temp[1]['mobile'] = isset($ip_api['mobile']) ? $ip_api['mobile'] : '';
$temp[1]['proxy'] = isset($ip_api['proxy']) ? $ip_api['proxy'] : '';
$temp[1]['isp'] = isset($ip_api['isp']) ? $ip_api['isp'] : '';
$temp[1]['org'] = isset($ip_api['org']) ? $ip_api['org'] : '';
$temp[1]['as'] = isset($ip_api['as']) ? $ip_api['as'] : '';

// Get geolocation data from ipinfo.io
$ipinfo = json_decode(file_get_contents('https://ipinfo.io/'.$ip.'/json', 0, $ctx), true);
$temp[2]['city'] = isset($ipinfo['city']) ? $ipinfo['city'] : '';
$temp[2]['region_name'] = isset($ipinfo['region']) ? $ipinfo['region'] : '';
$temp[2]['country_name'] = isset($ipinfo['country']) ? $ipinfo['country'] : '';
$temp[2]['country_code'] = isset($ipinfo['country']) ? $ipinfo['country'] : '';
$temp[2]['org'] = isset($ipinfo['org']) ? $ipinfo['org'] : '';

// Get geolocation data from freegeoip.net
$freegeoip = json_decode(file_get_contents('https://freegeoip.net/json/'.$ip, 0, $ctx), true);
$temp[3]['city'] = isset($freegeoip['city']) ? $freegeoip['city'] : '';
$temp[3]['region_name'] = isset($freegeoip['region_name']) ? $freegeoip['region_name'] : '';
$temp[3]['country_name'] = isset($freegeoip['country_name']) ? $freegeoip['country_name'] : '';
$temp[3]['country_code'] = isset($freegeoip['country_code']) ? $freegeoip['country_code'] : '';
$temp[3]['timezone'] = isset($freegeoip['timezone']) ? $freegeoip['timezone'] : '';
$temp[3]['latitude'] = isset($freegeoip['latitude']) ? $freegeoip['latitude'] : '';
$temp[3]['longitude'] = isset($freegeoip['longitude']) ? $freegeoip['longitude'] : '';

// Get geolocation data from ip.pentestit.ru
$ippentestit = json_decode(file_get_contents('http://ip.pentestit.ru/json.php?ip='.$ip, 0, $ctx), true);
$temp[4]['city'] = isset($ippentestit['city']) ? $ippentestit['city'] : '';
$temp[4]['region_name'] = isset($ippentestit['region_name']) ? $ippentestit['region_name'] : '';
$temp[4]['country_name'] = isset($ippentestit['country']) ? $ippentestit['country'] : '';
$temp[4]['country_code'] = isset($ippentestit['country']) ? $ippentestit['country'] : '';
$temp[4]['timezone'] = isset($ippentestit['timezone']) ? $ippentestit['timezone'] : '';
$temp[4]['latitude'] = isset($ippentestit['latitude']) ? $ippentestit['latitude'] : '';
$temp[4]['longitude'] = isset($ippentestit['longitude']) ? $ippentestit['longitude'] : '';

// var_dump($temp[4]);
// die();

$cities = [];
$regions_name = [];
$countryes_name = [];
$countryes_code = [];
$latitudes = [];
$longitudes = [];
$timezones = [];
$isps = [];
$orgs = [];
$ass = [];

array_walk($temp, function ($item) use (&$cities, &$regions_name, &$countryes_name, &$countryes_code, &$latitudes, &$longitudes, &$timezones, &$isps, &$orgs, &$ass) {
	if (!in_array($item['city'], $cities)) {
		$cities[] = $item['city'];
	}
	if (!in_array($item['region_name'], $regions_name) && !empty($item['region_name']) && !is_null($item['region_name'])) {
		$regions_name[] = $item['region_name'];
	}
	if (!in_array($item['country_name'], $countryes_name) && !empty($item['country_name']) && !is_null($item['country_name'])) {
		$countryes_name[] = $item['country_name'];
	}
	if (!in_array($item['country_code'], $countryes_code) && !empty($item['country_code']) && !is_null($item['country_code'])) {
		$countryes_code[] = $item['country_code'];
	}
	if (!in_array($item['latitude'], $latitudes) && !empty($item['latitude']) && !is_null($item['latitude'])) {
		$latitudes[] = $item['latitude'];
	}
	if (!in_array($item['longitude'], $longitudes) && !empty($item['longitude']) && !is_null($item['longitude'])) {
		$longitudes[] = $item['longitude'];
	}
	if (!in_array($item['timezone'], $timezones) && !empty($item['timezone']) && !is_null($item['timezone'])) {
		$timezones[] = $item['timezone'];
	}
	if (!in_array($item['isp'], $isps) && !empty($item['isp']) && !is_null($item['isp'])) {
		$isps[] = $item['isp'];
	}
	if (!in_array($item['org'], $orgs) && !empty($item['org']) && !is_null($item['org'])) {
		$orgs[] = $item['org'];
	}
	if (!in_array($item['as'], $ass) && !empty($item['as']) && !is_null($item['as'])) {
		$ass[] = $item['as'];
	}
});


$isps_str = implode(' ', $isps);
$orgs_str = implode(' ', $orgs);
$vpnCompany = ["Privax", "Avast", "VPN"];
$serverCompany = ["DigitalOcean", "Amazon", "QuadraNet"];

$authority = 100;

foreach ($vpnCompany as $value) {
	if (strpos($isps_str, $value) !== false) { $authority -= 50; break; }
} 

foreach ($serverCompany as $value) {
	if (strpos($orgs_str, $value) !== false) { $authority -= 20; break; }
} 


$approximateLocation = [
	 "ip" => $ip,
	 "city" => $cities,
	 "region_name" => $regions_name,
	 "country_name" => $countryes_name,
	 "country_code" => $countryes_code,
	 "latitude" => $latitudes,
	 "longitude" => $longitudes,
	 "timezone" => $timezones,
	 "ispname" => $isps,
	 "orgname" => $orgs,
	 "ascompany" => $ass,
	 "ipauthority" => $authority,
	 // "gethostbyaddr" =>  gethostbyaddr($ip),
	 "workTime" => microtime(true) - $start,
];

$jsonApproximateLocation = json_encode($approximateLocation);

if(isset($_GET["json"])){
	echo $jsonApproximateLocation;
} else {
	echo <<<EOL
		<!doctype html>
		<html class="no-js">
		<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		  
		<meta name="robots" content="noindex, nofollow">  
		<title>IP information</title>
		 
		<style>
		  body {
		    font: 1em/1.4 sans-serif;
		    margin: auto;
		    max-width: 940px;
		    padding: 0 20px;
		  }
		  pre {
		    font-size: 1.2em;
		    font-family: monospace;
		  }
		  pre + pre {
		    background: #ff9;
		    padding: 1em;
		  }
		  
		  pre {
		  word-break: break-all; /* webkit */
		  word-wrap: break-word;
		  white-space: pre;
		  white-space: -moz-pre-wrap; /* fennec */
		  white-space: pre-wrap;
		  white-space: pre\9; /* IE7+ */
		}

		textarea
		{
		  width:100%;
		}
		.textwrapper
		{
		  border:1px solid #999999;
		  margin:5px 0;
		  padding:3px;
		}
		</style>
		</head>
		  
		<body> 
EOL;

echo '<h2>Information about '.$approximateLocation['ip'].'</h2>';

echo '<h3>Region</h3>';
echo '<b>Country: </b>'.json_encode($approximateLocation['country_name']).' ('.json_encode($approximateLocation['country_code']).')</br>';
echo '<b>Region: </b>'.json_encode($approximateLocation['region_name']).'</br>';
echo '<b>City: </b>'.json_encode($approximateLocation['city']).'</br>';
echo '<b>Timezone: </b>'.json_encode($approximateLocation['timezone']).'</br>';
echo '<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?layer=mapnik&marker='.json_encode($approximateLocation['latitude'][0]).'%2C'.json_encode($approximateLocation['longitude'][0]).'" style="border: 1px solid black"></iframe><br/>';

echo '<h3>Company informaion (authority '.json_encode($approximateLocation['ipauthority']).'%)</h3>';
echo '<b>ISP name: </b>'.json_encode($approximateLocation['ispname']).'</br>';
echo '<b>Organisation name: </b>'.json_encode($approximateLocation['orgname']). ' (' .json_encode($approximateLocation['ascompany']).')</br>';

echo '<h3>Full information</h3>';
echo '<textarea rows="5">'. $jsonApproximateLocation.'</textarea>';


}
