# IP Detector

Example:
* For yourself information: https://127.0.0.1:666/ (for JSON-style output easily add `json` https://127.0.0.1:666?json)
* For check information about any IP: https://127.0.0.1:666/?ipToCheck=127.0.0.1 (for JSON-style output easily add `json` https://127.0.0.1:666/?json&ipToCheck=127.0.0.1)